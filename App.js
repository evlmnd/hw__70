import React from 'react';
import {StyleSheet, View} from 'react-native';
import Buttons from './components/Buttons/Buttons';
import Display from "./components/Display/Display";
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import reducer from './store/reducer';

const store = createStore(reducer);


export default class App extends React.Component {


    render() {
        return (
            <Provider store={store}>
                <View style={styles.container}>
                    <Display/>
                    <Buttons/>
                </View>
            </Provider>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

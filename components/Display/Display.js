import React, {Component, Fragment} from 'react';
import {StyleSheet, Text, View} from "react-native";
import {connect} from 'react-redux';

class Display extends Component {

    render() {


        return (
            <Fragment>
                <View style={styles.input}><Text style={styles.inputText} numberOfLines={5}>{this.props.input}</Text></View>
                <View style={styles.result}><Text style={styles.resultText}>{this.props.result}</Text></View>
            </Fragment>
        );
    }
}
const styles = StyleSheet.create({
    input: {
        flex: 3,
        backgroundColor: 'black',
        justifyContent: 'flex-end',
        flexDirection: 'column',
    },
    inputText: {
        color: 'rgba(256,256,256,0.7)',
        fontSize: 40,
        paddingTop: 10,
        flexWrap: 'wrap',
        textAlign: 'right',

    },
    result: {
        flex: 2,
        backgroundColor: 'black',
        alignItems: 'flex-end',

    },
    resultText: {
        flex: 2,
        color: '#fff',
        fontSize: 80,
    },
});

const mapStateToProps = state => {
    return {
        input: state.input,
        result: state.result
    };
};


export default connect(mapStateToProps)(Display);
import React, {Component} from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from "react-native";
import {connect} from 'react-redux';



class Buttons extends Component {

    render() {
        const buttons = (
            <View style={styles.buttons}>
                <View style={styles.numbers}>
                    <View style={styles.row}>
                        <TouchableOpacity style={styles.number}  onPress={() => this.props.pressButton(1)}><Text style={styles.numbertext}>1</Text></TouchableOpacity>
                        <TouchableOpacity style={styles.number}  onPress={() => this.props.pressButton(2)}><Text style={styles.numbertext}>2</Text></TouchableOpacity>
                        <TouchableOpacity style={styles.number}  onPress={() => this.props.pressButton(3)}><Text style={styles.numbertext}>3</Text></TouchableOpacity>
                    </View>
                    <View style={styles.row}>
                        <TouchableOpacity style={styles.number}  onPress={() => this.props.pressButton(4)}><Text style={styles.numbertext}>4</Text></TouchableOpacity>
                        <TouchableOpacity style={styles.number}  onPress={() => this.props.pressButton(5)}><Text style={styles.numbertext}>5</Text></TouchableOpacity>
                        <TouchableOpacity style={styles.number}  onPress={() => this.props.pressButton(6)}><Text style={styles.numbertext}>6</Text></TouchableOpacity>
                    </View>
                    <View style={styles.row}>
                        <TouchableOpacity style={styles.number}  onPress={() => this.props.pressButton(7)}><Text style={styles.numbertext}>7</Text></TouchableOpacity>
                        <TouchableOpacity style={styles.number}  onPress={() => this.props.pressButton(8)}><Text style={styles.numbertext}>8</Text></TouchableOpacity>
                        <TouchableOpacity style={styles.number}  onPress={() => this.props.pressButton(9)}><Text style={styles.numbertext}>9</Text></TouchableOpacity>
                    </View>
                    <View style={styles.row}>
                        <TouchableOpacity style={styles.number}  onPress={() => this.props.pressButton('.')}><Text style={styles.numbertext}>.</Text></TouchableOpacity>
                        <TouchableOpacity style={styles.number}  onPress={() => this.props.pressButton(0)}><Text style={styles.numbertext}>0</Text></TouchableOpacity>
                        <TouchableOpacity style={styles.number}  onPress={() => this.props.getResult()}><Text style={styles.numbertext}>=</Text></TouchableOpacity>
                    </View>
                </View>
                <View style={styles.actions}>
                        <TouchableOpacity style={styles.action}  onPress={() => this.props.reset()}><Text style={styles.actiontext}>CE</Text></TouchableOpacity>
                        <TouchableOpacity style={styles.action}  onPress={() => this.props.pressButton('+')}><Text style={styles.actiontext}>+</Text></TouchableOpacity>
                        <TouchableOpacity style={styles.action}  onPress={() => this.props.pressButton('-')}><Text style={styles.actiontext}>-</Text></TouchableOpacity>
                        <TouchableOpacity style={styles.action}  onPress={() => this.props.pressButton('*')}><Text style={styles.actiontext}>*</Text></TouchableOpacity>
                        <TouchableOpacity style={styles.action}  onPress={() => this.props.pressButton('/')}><Text style={styles.actiontext}>/</Text></TouchableOpacity>
                        <TouchableOpacity style={styles.action}  onPress={() => this.props.remove()}><Text style={styles.actiontext}>&lt;</Text></TouchableOpacity>
                </View>
            </View>


        );

        return (
            buttons
        )
    }
}

const styles = StyleSheet.create({
    buttons: {
        flex: 7,
        flexDirection: 'row'
    },
    row: {
        flex: 1,
        flexDirection: 'row',
    },
    numbers: {
        flex: 3,
        backgroundColor: 'rgba(0,0,0,0.3)',
    },
    actions: {
        flex: 1,
        flexDirection: 'column',
    },
    number: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.2)',
    },
    action: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ff942c',
        color: '#fff',
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.1)',
    },
    numbertext: {
        fontSize: 40,
    },
    actiontext: {
        fontSize: 40,
        color: '#fff',
    },
});

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => {
    return {
        pressButton: symbol => dispatch({type: 'PRESS_BUTTON', symbol}),
        reset: () => dispatch({type: 'RESET'}),
        remove: () => dispatch({type: 'REMOVE'}),
        getResult: () => dispatch({type: 'GET_RESULT'})
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Buttons);

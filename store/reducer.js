const initialState = {
    input: '1+1',
    result: '2'
};

const reducer = (state = initialState, action) => {
    switch(action.type){
        case 'PRESS_BUTTON':
            if (isNaN(state.input[state.input.length - 1]) && isNaN(action.symbol)) {
                return state;
            } else {
                return {...state, input: state.input + action.symbol};
            }
        case 'RESET':
            return {...state, input: '', result: ''};
        case 'REMOVE':
            const newInput = state.input.substring(0, state.input.length - 1);
            return {...state, input: newInput, result: ''};
        case 'GET_RESULT':
            try {
                let sum = eval(state.input).toString();
                return {...state, result: sum};
            } catch(e) {
                return {...state, result: 'error'};
            }
        default:
            return state;
    }
};

export default reducer;